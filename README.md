# FindMyDeviceServer

This server is able to communicate with FMD and save the latest location encrypted on it.

# Quick Setup
sudo apt-get install golang
 
git clone https://gitlab.com/nulide/findmydeviceserver.git

cd findmydeviceserver/src

sudo go run fmdserver.go -d ./ #root as ports are under 1024


